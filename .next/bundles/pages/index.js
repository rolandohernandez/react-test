
          window.__NEXT_REGISTER_PAGE('/', function() {
            var comp = module.exports =
webpackJsonp([5],{

/***/ 542:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(543);


/***/ }),

/***/ 543:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__resourceQuery) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(11);

var _react2 = _interopRequireDefault(_react);

var _main = __webpack_require__(544);

var _main2 = _interopRequireDefault(_main);

var _head = __webpack_require__(226);

var _head2 = _interopRequireDefault(_head);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = '/Users/rolandohernandez/Documents/Repositories/Personal/react-test/pages/index.js?entry';

exports.default = function () {
  return _react2.default.createElement('div', {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    }
  }, _react2.default.createElement(_head2.default, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    }
  }, _react2.default.createElement('title', {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, 'React Test'), _react2.default.createElement('meta', { charSet: 'utf-8', __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    }
  }), _react2.default.createElement('link', { rel: 'stylesheet', href: '/static/main.css', __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    }
  }), _react2.default.createElement('meta', { name: 'viewport', content: 'initial-scale=1.0, width=device-width', __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    }
  }), _react2.default.createElement('link', { href: 'https://fonts.googleapis.com/css?family=Droid+Sans:400,700', rel: 'stylesheet', __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    }
  })), _react2.default.createElement(_main2.default, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    }
  }));
};

 ;(function register() { /* react-hot-loader/webpack */ if (true) { if (typeof __REACT_HOT_LOADER__ === 'undefined') { return; } if (typeof module.exports === 'function') { __REACT_HOT_LOADER__.register(module.exports, 'module.exports', "/Users/rolandohernandez/Documents/Repositories/Personal/react-test/pages/index.js"); return; } for (var key in module.exports) { if (!Object.prototype.hasOwnProperty.call(module.exports, key)) { continue; } var namedExport = void 0; try { namedExport = module.exports[key]; } catch (err) { continue; } __REACT_HOT_LOADER__.register(namedExport, key, "/Users/rolandohernandez/Documents/Repositories/Personal/react-test/pages/index.js"); } } })();
    (function (Component, route) {
      if (false) return
      if (false) return

      var qs = __webpack_require__(69)
      var params = qs.parse(__resourceQuery.slice(1))
      if (params.entry == null) return

      module.hot.accept()
      Component.__route = route

      if (module.hot.status() === 'idle') return

      var components = next.router.components
      for (var r in components) {
        if (!components.hasOwnProperty(r)) continue

        if (components[r].Component.__route === route) {
          next.router.update(r, Component)
        }
      }
    })(module.exports.default || module.exports, "/")
  
/* WEBPACK VAR INJECTION */}.call(exports, "?entry"))

/***/ }),

/***/ 544:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(46);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(14);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(15);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(47);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(48);

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(11);

var _react2 = _interopRequireDefault(_react);

var _header = __webpack_require__(545);

var _header2 = _interopRequireDefault(_header);

var _request = __webpack_require__(553);

var _request2 = _interopRequireDefault(_request);

var _requestForm = __webpack_require__(555);

var _requestForm2 = _interopRequireDefault(_requestForm);

var _nextReduxWrapper = __webpack_require__(540);

var _nextReduxWrapper2 = _interopRequireDefault(_nextReduxWrapper);

var _reactRedux = __webpack_require__(546);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = '/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/main.js';


var initialRequests = [{
  name: "Nexus tecnologías informáticas S.A.",
  number: 456421654265,
  cuit: "33-70996436-0",
  date: "15/09/2017",
  terminal: ""
}];

// Redux Connect Attempt


var Main = function (_React$Component) {
  (0, _inherits3.default)(Main, _React$Component);

  function Main() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Main);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Main.__proto__ || (0, _getPrototypeOf2.default)(Main)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      requests: initialRequests,
      modalIsOpen: false
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Main, [{
    key: 'onCreateNewRequest',
    value: function onCreateNewRequest(request) {
      var currentRequests = this.state.requests;

      currentRequests.push(request);

      this.setState({
        requests: currentRequests,
        modalIsOpen: false
      });
    }
  }, {
    key: 'onOpenCreateModal',
    value: function onOpenCreateModal() {
      this.setState({
        modalIsOpen: true
      });
    }
  }, {
    key: 'onCloseCreateModal',
    value: function onCloseCreateModal() {
      this.setState({
        modalIsOpen: false
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement('div', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 55
        }
      }, _react2.default.createElement(_header2.default, { onOpenModal: this.onOpenCreateModal.bind(this), __source: {
          fileName: _jsxFileName,
          lineNumber: 57
        }
      }), _react2.default.createElement(_requestForm2.default, {
        isOpen: this.state.modalIsOpen,
        onCloseCreateModal: this.onCloseCreateModal.bind(this),
        onCreateNewRequest: this.onCreateNewRequest.bind(this), __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        }
      }), this.state.requests.map(function (request, index) {
        return _react2.default.createElement(_request2.default, {
          key: index,
          id: index,
          name: request.name,
          number: request.number,
          cuit: request.cuit,
          date: request.date, __source: {
            fileName: _jsxFileName,
            lineNumber: 65
          }
        });
      }));
    }
  }]);

  return Main;
}(_react2.default.Component);

exports.default = Main;

 ;(function register() { /* react-hot-loader/webpack */ if (true) { if (typeof __REACT_HOT_LOADER__ === 'undefined') { return; } if (typeof module.exports === 'function') { __REACT_HOT_LOADER__.register(module.exports, 'module.exports', "/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/main.js"); return; } for (var key in module.exports) { if (!Object.prototype.hasOwnProperty.call(module.exports, key)) { continue; } var namedExport = void 0; try { namedExport = module.exports[key]; } catch (err) { continue; } __REACT_HOT_LOADER__.register(namedExport, key, "/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/main.js"); } } })();

/***/ }),

/***/ 545:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(11);

var _react2 = _interopRequireDefault(_react);

var _nextReduxWrapper = __webpack_require__(540);

var _nextReduxWrapper2 = _interopRequireDefault(_nextReduxWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = "/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/header.js";

exports.default = function (_ref) {
  var onOpenModal = _ref.onOpenModal;
  return _react2.default.createElement("header", { className: "header", __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    }
  }, _react2.default.createElement("div", { className: "header-title", __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    }
  }, _react2.default.createElement("img", { src: "./static/images/visa.png", alt: "", __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    }
  }), _react2.default.createElement("h2", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, "Solicitudes")), _react2.default.createElement("button", { onClick: onOpenModal, __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    }
  }, "Crear Solicitud"));
};

 ;(function register() { /* react-hot-loader/webpack */ if (true) { if (typeof __REACT_HOT_LOADER__ === 'undefined') { return; } if (typeof module.exports === 'function') { __REACT_HOT_LOADER__.register(module.exports, 'module.exports', "/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/header.js"); return; } for (var key in module.exports) { if (!Object.prototype.hasOwnProperty.call(module.exports, key)) { continue; } var namedExport = void 0; try { namedExport = module.exports[key]; } catch (err) { continue; } __REACT_HOT_LOADER__.register(namedExport, key, "/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/header.js"); } } })();

/***/ }),

/***/ 553:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = __webpack_require__(541);

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = __webpack_require__(46);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(14);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(15);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(47);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(48);

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(11);

var _react2 = _interopRequireDefault(_react);

var _api = __webpack_require__(554);

var _api2 = _interopRequireDefault(_api);

var _nextReduxWrapper = __webpack_require__(540);

var _nextReduxWrapper2 = _interopRequireDefault(_nextReduxWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = '/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/request.js';


var Request = function (_React$Component) {
  (0, _inherits3.default)(Request, _React$Component);

  function Request() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Request);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Request.__proto__ || (0, _getPrototypeOf2.default)(Request)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      terminal: _this.props.terminal,
      rejecting: false,
      valid: false,
      approved: false,
      rejected: false
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Request, [{
    key: 'approveRequest',
    value: function approveRequest() {
      this.setState({
        approved: true
      });
    }
  }, {
    key: 'rejectRequest',
    value: function rejectRequest() {
      this.setState({
        rejected: true
      });
    }
  }, {
    key: 'handleChange',
    value: function handleChange(event) {
      var input = event.target;

      this.setState((0, _defineProperty3.default)({
        valid: _api2.default.find(function (item) {
          return item.id === event.target.value;
        }.bind(this))
      }, input.name, input.value));
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement('div', { className: 'request ' + (this.state.valid ? 'is-valid' : '') + ' ' + (this.state.rejected ? 'is-rejected' : '') + ' ' + (this.state.approved ? 'is-approved' : ''), __source: {
          fileName: _jsxFileName,
          lineNumber: 37
        }
      }, this.state.rejecting ? _react2.default.createElement('div', { className: 'confirmation-message', __source: {
          fileName: _jsxFileName,
          lineNumber: 40
        }
      }, _react2.default.createElement('p', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 41
        }
      }, '\xBFEsta\u0301s seguro de rechazar la solicitud de este comercio ?'), _react2.default.createElement('div', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 42
        }
      }, _react2.default.createElement('button', { onClick: function onClick() {
          return _this2.setState({ rejecting: false });
        }, __source: {
          fileName: _jsxFileName,
          lineNumber: 43
        }
      }, 'Cancelar'), _react2.default.createElement('button', { onClick: this.rejectRequest.bind(this), __source: {
          fileName: _jsxFileName,
          lineNumber: 44
        }
      }, 'Rechazar'))) : null, _react2.default.createElement('button', { onClick: function onClick() {
          return _this2.setState({ rejecting: !_this2.state.rejecting });
        }, className: 'request-btn request-btn-decline ' + (this.state.rejecting ? 'is-active' : ''), __source: {
          fileName: _jsxFileName,
          lineNumber: 49
        }
      }), _react2.default.createElement('div', { className: 'commerce-details', __source: {
          fileName: _jsxFileName,
          lineNumber: 51
        }
      }, _react2.default.createElement('h3', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 52
        }
      }, this.props.name), _react2.default.createElement('p', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 53
        }
      }, _react2.default.createElement('span', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 53
        }
      }, 'CUIT:'), ' ', this.props.cuit)), _react2.default.createElement('div', { className: 'commerce-details', __source: {
          fileName: _jsxFileName,
          lineNumber: 56
        }
      }, _react2.default.createElement('p', { className: 'commerce-title', __source: {
          fileName: _jsxFileName,
          lineNumber: 57
        }
      }, 'N\xBA de establecimiento'), _react2.default.createElement('p', { className: 'commerce-number', __source: {
          fileName: _jsxFileName,
          lineNumber: 58
        }
      }, this.props.number), _react2.default.createElement('p', { className: 'commerce-date', __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        }
      }, this.props.date)), _react2.default.createElement('input', {
        name: 'terminal',
        type: 'text',
        placeholder: 'N\xBA de Terminal',
        value: this.state.terminal,
        onChange: this.handleChange.bind(this),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 62
        }
      }), _react2.default.createElement('button', { disabled: !this.state.valid, onClick: this.approveRequest.bind(this), className: 'request-btn request-btn-accept', __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        }
      }));
    }
  }]);

  return Request;
}(_react2.default.Component);

exports.default = Request;


Request.defaultProps = {
  id: "",
  name: "",
  number: "",
  date: "11/11/2017",
  cuit: "",
  terminal: ""
};

 ;(function register() { /* react-hot-loader/webpack */ if (true) { if (typeof __REACT_HOT_LOADER__ === 'undefined') { return; } if (typeof module.exports === 'function') { __REACT_HOT_LOADER__.register(module.exports, 'module.exports', "/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/request.js"); return; } for (var key in module.exports) { if (!Object.prototype.hasOwnProperty.call(module.exports, key)) { continue; } var namedExport = void 0; try { namedExport = module.exports[key]; } catch (err) { continue; } __REACT_HOT_LOADER__.register(namedExport, key, "/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/request.js"); } } })();

/***/ }),

/***/ 554:
/***/ (function(module, exports) {

module.exports = [
	{
		"enabled": false,
		"id": "SCK0000"
	},
	{
		"enabled": false,
		"id": "SCK0001"
	},
	{
		"enabled": false,
		"id": "SCK0002"
	},
	{
		"enabled": false,
		"id": "SCK0003"
	},
	{
		"enabled": true,
		"id": "SCK0004"
	},
	{
		"enabled": true,
		"id": "SCK0005"
	},
	{
		"enabled": true,
		"id": "SCK0006"
	},
	{
		"enabled": true,
		"id": "SCK0007"
	},
	{
		"enabled": true,
		"id": "SCK0008"
	},
	{
		"enabled": true,
		"id": "SCK0009"
	},
	{
		"enabled": true,
		"id": "SCK0010"
	},
	{
		"enabled": true,
		"id": "SCK0011"
	},
	{
		"enabled": true,
		"id": "SCK0012"
	},
	{
		"enabled": true,
		"id": "SCK0013"
	},
	{
		"enabled": true,
		"id": "SCK0014"
	},
	{
		"enabled": true,
		"id": "SCK0015"
	},
	{
		"enabled": true,
		"id": "SCK0016"
	},
	{
		"enabled": true,
		"id": "SCK0017"
	},
	{
		"enabled": true,
		"id": "SCK0018"
	},
	{
		"enabled": true,
		"id": "SCK0019"
	},
	{
		"enabled": true,
		"id": "SCK0020"
	},
	{
		"enabled": true,
		"id": "SCK0021"
	},
	{
		"enabled": true,
		"id": "SCK0022"
	},
	{
		"enabled": true,
		"id": "SCK0023"
	},
	{
		"enabled": true,
		"id": "SCK0024"
	},
	{
		"enabled": true,
		"id": "SCK0025"
	},
	{
		"enabled": true,
		"id": "SCK0026"
	},
	{
		"enabled": true,
		"id": "SCK0027"
	},
	{
		"enabled": false,
		"id": "SCK0028"
	},
	{
		"enabled": false,
		"id": "SCK0029"
	},
	{
		"enabled": false,
		"id": "SCK0030"
	},
	{
		"enabled": false,
		"id": "SCK0031"
	},
	{
		"enabled": false,
		"id": "SCK0032"
	},
	{
		"enabled": false,
		"id": "SCK0033"
	},
	{
		"enabled": false,
		"id": "SCK0034"
	},
	{
		"enabled": false,
		"id": "SCK0035"
	},
	{
		"enabled": false,
		"id": "SCK0036"
	},
	{
		"enabled": false,
		"id": "SCK0037"
	},
	{
		"enabled": false,
		"id": "SCK0038"
	},
	{
		"enabled": true,
		"id": "SCK0039"
	},
	{
		"enabled": true,
		"id": "SCK0040"
	},
	{
		"enabled": true,
		"id": "SCK0041"
	},
	{
		"enabled": true,
		"id": "SCK0042"
	},
	{
		"enabled": true,
		"id": "SCK0043"
	},
	{
		"enabled": true,
		"id": "SCK0044"
	},
	{
		"enabled": true,
		"id": "SCK0045"
	},
	{
		"enabled": true,
		"id": "SCK0046"
	},
	{
		"enabled": true,
		"id": "SCK0047"
	},
	{
		"enabled": true,
		"id": "SCK0048"
	},
	{
		"enabled": true,
		"id": "SCK0049"
	},
	{
		"enabled": true,
		"id": "SCK0050"
	},
	{
		"enabled": true,
		"id": "SCK0051"
	},
	{
		"enabled": true,
		"id": "SCK0052"
	},
	{
		"enabled": true,
		"id": "SCK0053"
	},
	{
		"enabled": true,
		"id": "SCK0054"
	},
	{
		"enabled": true,
		"id": "SCK0055"
	},
	{
		"enabled": true,
		"id": "SCK0056"
	},
	{
		"enabled": true,
		"id": "SCK0057"
	},
	{
		"enabled": true,
		"id": "SCK0058"
	},
	{
		"enabled": true,
		"id": "SCK0059"
	},
	{
		"enabled": true,
		"id": "SCK0060"
	},
	{
		"enabled": true,
		"id": "SCK0061"
	},
	{
		"enabled": true,
		"id": "SCK0062"
	},
	{
		"enabled": true,
		"id": "SCK0063"
	},
	{
		"enabled": true,
		"id": "SCK0064"
	},
	{
		"enabled": true,
		"id": "SCK0065"
	},
	{
		"enabled": true,
		"id": "SCK0066"
	},
	{
		"enabled": true,
		"id": "SCK0067"
	},
	{
		"enabled": true,
		"id": "SCK0068"
	},
	{
		"enabled": true,
		"id": "SCK0069"
	},
	{
		"enabled": true,
		"id": "SCK0070"
	},
	{
		"enabled": true,
		"id": "SCK0071"
	},
	{
		"enabled": true,
		"id": "SCK0072"
	},
	{
		"enabled": true,
		"id": "SCK0073"
	},
	{
		"enabled": true,
		"id": "SCK0074"
	},
	{
		"enabled": true,
		"id": "SCK0075"
	},
	{
		"enabled": true,
		"id": "SCK0076"
	},
	{
		"enabled": true,
		"id": "SCK0077"
	},
	{
		"enabled": true,
		"id": "SCK0078"
	},
	{
		"enabled": true,
		"id": "SCK0079"
	},
	{
		"enabled": true,
		"id": "SCK0080"
	},
	{
		"enabled": true,
		"id": "SCK0081"
	},
	{
		"enabled": true,
		"id": "SCK0082"
	},
	{
		"enabled": true,
		"id": "SCK0083"
	},
	{
		"enabled": true,
		"id": "SCK0084"
	},
	{
		"enabled": true,
		"id": "SCK0085"
	},
	{
		"enabled": true,
		"id": "SCK0086"
	},
	{
		"enabled": true,
		"id": "SCK0087"
	},
	{
		"enabled": true,
		"id": "SCK0088"
	},
	{
		"enabled": true,
		"id": "SCK0089"
	},
	{
		"enabled": true,
		"id": "SCK0090"
	},
	{
		"enabled": true,
		"id": "SCK0091"
	},
	{
		"enabled": true,
		"id": "SCK0092"
	},
	{
		"enabled": true,
		"id": "SCK0093"
	},
	{
		"enabled": true,
		"id": "SCK0094"
	},
	{
		"enabled": true,
		"id": "SCK0095"
	},
	{
		"enabled": true,
		"id": "SCK0096"
	},
	{
		"enabled": true,
		"id": "SCK0097"
	},
	{
		"enabled": true,
		"id": "SCK0098"
	},
	{
		"enabled": true,
		"id": "SCK0099"
	}
];

/***/ }),

/***/ 555:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = __webpack_require__(541);

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = __webpack_require__(46);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(14);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(15);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(47);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(48);

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(11);

var _react2 = _interopRequireDefault(_react);

var _nextReduxWrapper = __webpack_require__(540);

var _nextReduxWrapper2 = _interopRequireDefault(_nextReduxWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = "/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/request-form.js";


var RequestForm = function (_React$Component) {
  (0, _inherits3.default)(RequestForm, _React$Component);

  function RequestForm() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, RequestForm);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = RequestForm.__proto__ || (0, _getPrototypeOf2.default)(RequestForm)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      name: "",
      cuit: "",
      number: ""
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(RequestForm, [{
    key: "handleChange",
    value: function handleChange(event) {
      var input = event.target;

      this.setState((0, _defineProperty3.default)({}, input.name, input.value));
    }
  }, {
    key: "handleSubmit",
    value: function handleSubmit(event) {
      event.preventDefault();

      var c = this.state;

      if (c.name.length < 1 || c.cuit.length < 1 || c.number.length < 1) {
        return alert('Por favor llena todos los campos.');
      }

      this.props.onCreateNewRequest(this.state);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement("div", { className: "form-overlay " + (this.props.isOpen ? 'is-open' : ''), __source: {
          fileName: _jsxFileName,
          lineNumber: 33
        }
      }, _react2.default.createElement("form", { className: "request-form", onSubmit: this.handleSubmit.bind(this), __source: {
          fileName: _jsxFileName,
          lineNumber: 34
        }
      }, _react2.default.createElement("h2", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 35
        }
      }, "Crear solicitud"), _react2.default.createElement("div", { className: "form-fields", __source: {
          fileName: _jsxFileName,
          lineNumber: 36
        }
      }, _react2.default.createElement("label", { htmlFor: "name", __source: {
          fileName: _jsxFileName,
          lineNumber: 38
        }
      }, "Razo\u0301n Social"), _react2.default.createElement("input", {
        name: "name",
        type: "text",
        placeholder: "ej: Nexus S.A",
        onChange: this.handleChange.bind(this),
        value: this.state.name,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 39
        }
      }), _react2.default.createElement("div", { className: "fields-cols", __source: {
          fileName: _jsxFileName,
          lineNumber: 47
        }
      }, _react2.default.createElement("div", { className: "fields-col", __source: {
          fileName: _jsxFileName,
          lineNumber: 48
        }
      }, _react2.default.createElement("label", { htmlFor: "cuit", __source: {
          fileName: _jsxFileName,
          lineNumber: 49
        }
      }, "Nu\u0301mero de CUIT"), _react2.default.createElement("input", {
        name: "cuit",
        type: "text",
        placeholder: "00-000000-0",
        onChange: this.handleChange.bind(this),
        value: this.state.cuit,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 50
        }
      })), _react2.default.createElement("div", { className: "fields-col", __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        }
      }, _react2.default.createElement("label", { htmlFor: "number", __source: {
          fileName: _jsxFileName,
          lineNumber: 60
        }
      }, "Nu\u0301mero de establecimiento"), _react2.default.createElement("input", {
        name: "number",
        type: "text",
        placeholder: "00-000000-0",
        onChange: this.handleChange.bind(this),
        value: this.state.number,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 61
        }
      })))), _react2.default.createElement("div", { className: "form-buttons", __source: {
          fileName: _jsxFileName,
          lineNumber: 72
        }
      }, _react2.default.createElement("input", { type: "button", value: "Cancel", onClick: function onClick() {
          _this2.props.onCloseCreateModal();
        }, __source: {
          fileName: _jsxFileName,
          lineNumber: 73
        }
      }), _react2.default.createElement("input", { type: "submit", value: "Crear", __source: {
          fileName: _jsxFileName,
          lineNumber: 76
        }
      }))));
    }
  }]);

  return RequestForm;
}(_react2.default.Component);

exports.default = RequestForm;

 ;(function register() { /* react-hot-loader/webpack */ if (true) { if (typeof __REACT_HOT_LOADER__ === 'undefined') { return; } if (typeof module.exports === 'function') { __REACT_HOT_LOADER__.register(module.exports, 'module.exports', "/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/request-form.js"); return; } for (var key in module.exports) { if (!Object.prototype.hasOwnProperty.call(module.exports, key)) { continue; } var namedExport = void 0; try { namedExport = module.exports[key]; } catch (err) { continue; } __REACT_HOT_LOADER__.register(namedExport, key, "/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/request-form.js"); } } })();

/***/ })

},[542]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVuZGxlcy9wYWdlcy9pbmRleC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3BhZ2VzPzcxYWY0NDUiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9tYWluLmpzPzcxYWY0NDUiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9oZWFkZXIuanM/NzFhZjQ0NSIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL3JlcXVlc3QuanM/NzFhZjQ0NSIsIndlYnBhY2s6Ly8vLi9zdGF0aWMvYXBpLmpzb24/NzFhZjQ0NSIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL3JlcXVlc3QtZm9ybS5qcz83MWFmNDQ1Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBNYWluIGZyb20gJy4uL2NvbXBvbmVudHMvbWFpbidcbmltcG9ydCBIZWFkIGZyb20gJ25leHQvaGVhZCdcblxuZXhwb3J0IGRlZmF1bHQgKCkgPT4gKFxuICA8ZGl2PlxuICAgIDxIZWFkPlxuICAgICAgPHRpdGxlPlJlYWN0IFRlc3Q8L3RpdGxlPlxuICAgICAgPG1ldGEgY2hhclNldD0ndXRmLTgnIC8+XG4gICAgICA8bGluayByZWw9XCJzdHlsZXNoZWV0XCIgaHJlZj1cIi9zdGF0aWMvbWFpbi5jc3NcIiAvPlxuICAgICAgPG1ldGEgbmFtZT0ndmlld3BvcnQnIGNvbnRlbnQ9J2luaXRpYWwtc2NhbGU9MS4wLCB3aWR0aD1kZXZpY2Utd2lkdGgnIC8+XG4gICAgICA8bGluayBocmVmPVwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PURyb2lkK1NhbnM6NDAwLDcwMFwiIHJlbD1cInN0eWxlc2hlZXRcIiAvPlxuICAgIDwvSGVhZD5cblxuICAgIDxNYWluIC8+XG4gIDwvZGl2PlxuKVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcGFnZXM/ZW50cnkiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IEhlYWRlciBmcm9tICcuL2hlYWRlcidcbmltcG9ydCBSZXF1ZXN0IGZyb20gJy4vcmVxdWVzdCdcbmltcG9ydCBSZXF1ZXN0Rm9ybSBmcm9tICcuL3JlcXVlc3QtZm9ybSdcbmltcG9ydCB3aXRoUmVkdXggZnJvbSAnbmV4dC1yZWR1eC13cmFwcGVyJ1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4J1xuXG5jb25zdCBpbml0aWFsUmVxdWVzdHMgPSBbXG4gIHtcbiAgICBuYW1lOiBcIk5leHVzIHRlY25vbG9nacyBYXMgaW5mb3JtYcyBdGljYXMgUy5BLlwiLFxuICAgIG51bWJlcjogNDU2NDIxNjU0MjY1LFxuICAgIGN1aXQ6IFwiMzMtNzA5OTY0MzYtMFwiLFxuICAgIGRhdGU6IFwiMTUvMDkvMjAxN1wiLFxuICAgIHRlcm1pbmFsOiBcIlwiXG4gIH1cbl1cblxuLy8gUmVkdXggQ29ubmVjdCBBdHRlbXB0XG5cblxuXG5cblxuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNYWluIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgc3RhdGUgPSB7XG4gICAgcmVxdWVzdHM6IGluaXRpYWxSZXF1ZXN0cyxcbiAgICBtb2RhbElzT3BlbjogZmFsc2VcbiAgfVxuXG4gIG9uQ3JlYXRlTmV3UmVxdWVzdChyZXF1ZXN0KSB7XG4gICAgbGV0IGN1cnJlbnRSZXF1ZXN0cyA9IHRoaXMuc3RhdGUucmVxdWVzdHM7XG5cbiAgICBjdXJyZW50UmVxdWVzdHMucHVzaChyZXF1ZXN0KTtcblxuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgcmVxdWVzdHM6IGN1cnJlbnRSZXF1ZXN0cyxcbiAgICAgIG1vZGFsSXNPcGVuOiBmYWxzZVxuICAgIH0pO1xuICB9XG5cbiAgb25PcGVuQ3JlYXRlTW9kYWwoKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBtb2RhbElzT3BlbjogdHJ1ZVxuICAgIH0pO1xuICB9XG5cbiAgb25DbG9zZUNyZWF0ZU1vZGFsKCkge1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgbW9kYWxJc09wZW46IGZhbHNlXG4gICAgfSk7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIDxkaXY+XG5cbiAgICAgIDxIZWFkZXIgb25PcGVuTW9kYWw9e3RoaXMub25PcGVuQ3JlYXRlTW9kYWwuYmluZCh0aGlzKX0gLz5cblxuICAgICAgPFJlcXVlc3RGb3JtXG4gICAgICAgIGlzT3Blbj17dGhpcy5zdGF0ZS5tb2RhbElzT3Blbn1cbiAgICAgICAgb25DbG9zZUNyZWF0ZU1vZGFsPXt0aGlzLm9uQ2xvc2VDcmVhdGVNb2RhbC5iaW5kKHRoaXMpfVxuICAgICAgICBvbkNyZWF0ZU5ld1JlcXVlc3Q9e3RoaXMub25DcmVhdGVOZXdSZXF1ZXN0LmJpbmQodGhpcyl9ICAvPlxuXG4gICAgICB7dGhpcy5zdGF0ZS5yZXF1ZXN0cy5tYXAoKHJlcXVlc3QsIGluZGV4KSA9PlxuICAgICAgICA8UmVxdWVzdFxuICAgICAgICAgIGtleT17aW5kZXh9XG4gICAgICAgICAgaWQ9e2luZGV4fVxuICAgICAgICAgIG5hbWU9e3JlcXVlc3QubmFtZX1cbiAgICAgICAgICBudW1iZXI9e3JlcXVlc3QubnVtYmVyfVxuICAgICAgICAgIGN1aXQ9e3JlcXVlc3QuY3VpdH1cbiAgICAgICAgICBkYXRlPXtyZXF1ZXN0LmRhdGV9IC8+XG4gICAgICApfVxuICAgIDwvZGl2PjtcbiAgfVxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vY29tcG9uZW50cy9tYWluLmpzIiwiaW1wb3J0IHdpdGhSZWR1eCBmcm9tIFwibmV4dC1yZWR1eC13cmFwcGVyXCI7XG5cbmV4cG9ydCBkZWZhdWx0ICh7b25PcGVuTW9kYWx9KSA9PiAoXG4gIDxoZWFkZXIgY2xhc3NOYW1lPVwiaGVhZGVyXCI+XG4gICAgPGRpdiBjbGFzc05hbWU9XCJoZWFkZXItdGl0bGVcIj5cbiAgICAgIDxpbWcgc3JjPVwiLi9zdGF0aWMvaW1hZ2VzL3Zpc2EucG5nXCIgYWx0PVwiXCIvPlxuICAgICAgPGgyPlNvbGljaXR1ZGVzPC9oMj5cbiAgICA8L2Rpdj5cbiAgICA8YnV0dG9uIG9uQ2xpY2s9e29uT3Blbk1vZGFsfT5DcmVhciBTb2xpY2l0dWQ8L2J1dHRvbj5cbiAgPC9oZWFkZXI+XG4pXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9jb21wb25lbnRzL2hlYWRlci5qcyIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCdcbmltcG9ydCB0ZXJtaW5hbERhdGEgZnJvbSAnLi4vc3RhdGljL2FwaS5qc29uJ1xuaW1wb3J0IHdpdGhSZWR1eCBmcm9tIFwibmV4dC1yZWR1eC13cmFwcGVyXCI7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFJlcXVlc3QgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBzdGF0ZSA9IHtcbiAgICB0ZXJtaW5hbDogdGhpcy5wcm9wcy50ZXJtaW5hbCxcbiAgICByZWplY3Rpbmc6IGZhbHNlLFxuICAgIHZhbGlkOiBmYWxzZSxcbiAgICBhcHByb3ZlZDogZmFsc2UsXG4gICAgcmVqZWN0ZWQ6IGZhbHNlXG4gIH07XG5cbiAgYXBwcm92ZVJlcXVlc3QoKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBhcHByb3ZlZDogdHJ1ZVxuICAgIH0pXG4gIH1cblxuICByZWplY3RSZXF1ZXN0KCkge1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgcmVqZWN0ZWQ6IHRydWVcbiAgICB9KVxuICB9XG5cbiAgaGFuZGxlQ2hhbmdlKGV2ZW50KSB7XG4gICAgY29uc3QgaW5wdXQgPSBldmVudC50YXJnZXQ7XG5cbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIHZhbGlkOiB0ZXJtaW5hbERhdGEuZmluZChmdW5jdGlvbihpdGVtKXsgcmV0dXJuIGl0ZW0uaWQgPT09IGV2ZW50LnRhcmdldC52YWx1ZSB9LmJpbmQodGhpcykpLFxuICAgICAgW2lucHV0Lm5hbWVdOiBpbnB1dC52YWx1ZVxuICAgIH0pXG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtgcmVxdWVzdCAkeyh0aGlzLnN0YXRlLnZhbGlkKSA/ICdpcy12YWxpZCcgOiAnJ30gJHsodGhpcy5zdGF0ZS5yZWplY3RlZCkgPyAnaXMtcmVqZWN0ZWQnIDogJyd9ICR7KHRoaXMuc3RhdGUuYXBwcm92ZWQpID8gJ2lzLWFwcHJvdmVkJyA6ICcnfWB9PlxuXG4gICAgICAgIHsodGhpcy5zdGF0ZS5yZWplY3RpbmcpID9cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbmZpcm1hdGlvbi1tZXNzYWdlXCI+XG4gICAgICAgICAgICA8cD7Cv0VzdGHMgXMgc2VndXJvIGRlIHJlY2hhemFyIGxhIHNvbGljaXR1ZCBkZSBlc3RlIGNvbWVyY2lvID88L3A+XG4gICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICA8YnV0dG9uIG9uQ2xpY2s9eygpID0+IHRoaXMuc2V0U3RhdGUoeyByZWplY3Rpbmc6IGZhbHNlIH0pfT5DYW5jZWxhcjwvYnV0dG9uPlxuICAgICAgICAgICAgICA8YnV0dG9uIG9uQ2xpY2s9e3RoaXMucmVqZWN0UmVxdWVzdC5iaW5kKHRoaXMpfT5SZWNoYXphcjwvYnV0dG9uPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDogbnVsbCB9XG5cbiAgICAgICAgPGJ1dHRvbiBvbkNsaWNrPXsoKSA9PiB0aGlzLnNldFN0YXRlKHsgcmVqZWN0aW5nOiAhdGhpcy5zdGF0ZS5yZWplY3RpbmcgfSl9IGNsYXNzTmFtZT17YHJlcXVlc3QtYnRuIHJlcXVlc3QtYnRuLWRlY2xpbmUgJHsodGhpcy5zdGF0ZS5yZWplY3RpbmcpID8gJ2lzLWFjdGl2ZScgOiAnJ31gfSA+PC9idXR0b24+XG5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb21tZXJjZS1kZXRhaWxzXCI+XG4gICAgICAgICAgPGgzPnt0aGlzLnByb3BzLm5hbWV9PC9oMz5cbiAgICAgICAgICA8cD48c3Bhbj5DVUlUOjwvc3Bhbj4ge3RoaXMucHJvcHMuY3VpdH08L3A+XG4gICAgICAgIDwvZGl2PlxuXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29tbWVyY2UtZGV0YWlsc1wiPlxuICAgICAgICAgIDxwIGNsYXNzTmFtZT1cImNvbW1lcmNlLXRpdGxlXCI+TsK6IGRlIGVzdGFibGVjaW1pZW50bzwvcD5cbiAgICAgICAgICA8cCBjbGFzc05hbWU9XCJjb21tZXJjZS1udW1iZXJcIj57dGhpcy5wcm9wcy5udW1iZXJ9PC9wPlxuICAgICAgICAgIDxwIGNsYXNzTmFtZT1cImNvbW1lcmNlLWRhdGVcIj57dGhpcy5wcm9wcy5kYXRlfTwvcD5cbiAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgPGlucHV0XG4gICAgICAgICAgbmFtZT1cInRlcm1pbmFsXCJcbiAgICAgICAgICB0eXBlPVwidGV4dFwiXG4gICAgICAgICAgcGxhY2Vob2xkZXI9XCJOwrogZGUgVGVybWluYWxcIlxuICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLnRlcm1pbmFsfVxuICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLmhhbmRsZUNoYW5nZS5iaW5kKHRoaXMpfVxuICAgICAgICAvPlxuXG4gICAgICAgIDxidXR0b24gZGlzYWJsZWQ9eyF0aGlzLnN0YXRlLnZhbGlkfSBvbkNsaWNrPXt0aGlzLmFwcHJvdmVSZXF1ZXN0LmJpbmQodGhpcyl9IGNsYXNzTmFtZT1cInJlcXVlc3QtYnRuIHJlcXVlc3QtYnRuLWFjY2VwdFwiPjwvYnV0dG9uPlxuICAgICAgPC9kaXY+XG4gICAgKVxuICB9XG59XG5cblJlcXVlc3QuZGVmYXVsdFByb3BzID0ge1xuICBpZDogXCJcIixcbiAgbmFtZTogXCJcIixcbiAgbnVtYmVyOiBcIlwiLFxuICBkYXRlOiBcIjExLzExLzIwMTdcIixcbiAgY3VpdDogXCJcIixcbiAgdGVybWluYWw6IFwiXCJcbn07XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9jb21wb25lbnRzL3JlcXVlc3QuanMiLCJtb2R1bGUuZXhwb3J0cyA9IFtcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiBmYWxzZSxcblx0XHRcImlkXCI6IFwiU0NLMDAwMFwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogZmFsc2UsXG5cdFx0XCJpZFwiOiBcIlNDSzAwMDFcIlxuXHR9LFxuXHR7XG5cdFx0XCJlbmFibGVkXCI6IGZhbHNlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDAyXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiBmYWxzZSxcblx0XHRcImlkXCI6IFwiU0NLMDAwM1wiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAwNFwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAwNVwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAwNlwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAwN1wiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAwOFwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAwOVwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAxMFwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAxMVwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAxMlwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAxM1wiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAxNFwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAxNVwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAxNlwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAxN1wiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAxOFwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAxOVwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAyMFwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAyMVwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAyMlwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAyM1wiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAyNFwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAyNVwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAyNlwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogdHJ1ZSxcblx0XHRcImlkXCI6IFwiU0NLMDAyN1wiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogZmFsc2UsXG5cdFx0XCJpZFwiOiBcIlNDSzAwMjhcIlxuXHR9LFxuXHR7XG5cdFx0XCJlbmFibGVkXCI6IGZhbHNlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDI5XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiBmYWxzZSxcblx0XHRcImlkXCI6IFwiU0NLMDAzMFwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogZmFsc2UsXG5cdFx0XCJpZFwiOiBcIlNDSzAwMzFcIlxuXHR9LFxuXHR7XG5cdFx0XCJlbmFibGVkXCI6IGZhbHNlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDMyXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiBmYWxzZSxcblx0XHRcImlkXCI6IFwiU0NLMDAzM1wiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogZmFsc2UsXG5cdFx0XCJpZFwiOiBcIlNDSzAwMzRcIlxuXHR9LFxuXHR7XG5cdFx0XCJlbmFibGVkXCI6IGZhbHNlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDM1XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiBmYWxzZSxcblx0XHRcImlkXCI6IFwiU0NLMDAzNlwiXG5cdH0sXG5cdHtcblx0XHRcImVuYWJsZWRcIjogZmFsc2UsXG5cdFx0XCJpZFwiOiBcIlNDSzAwMzdcIlxuXHR9LFxuXHR7XG5cdFx0XCJlbmFibGVkXCI6IGZhbHNlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDM4XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDM5XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDQwXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDQxXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDQyXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDQzXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDQ0XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDQ1XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDQ2XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDQ3XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDQ4XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDQ5XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDUwXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDUxXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDUyXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDUzXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDU0XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDU1XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDU2XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDU3XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDU4XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDU5XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDYwXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDYxXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDYyXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDYzXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDY0XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDY1XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDY2XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDY3XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDY4XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDY5XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDcwXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDcxXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDcyXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDczXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDc0XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDc1XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDc2XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDc3XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDc4XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDc5XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDgwXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDgxXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDgyXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDgzXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDg0XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDg1XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDg2XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDg3XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDg4XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDg5XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDkwXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDkxXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDkyXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDkzXCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDk0XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDk1XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDk2XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDk3XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDk4XCJcblx0fSxcblx0e1xuXHRcdFwiZW5hYmxlZFwiOiB0cnVlLFxuXHRcdFwiaWRcIjogXCJTQ0swMDk5XCJcblx0fVxuXTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3N0YXRpYy9hcGkuanNvblxuLy8gbW9kdWxlIGlkID0gNTU0XG4vLyBtb2R1bGUgY2h1bmtzID0gNSIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCdcbmltcG9ydCB3aXRoUmVkdXggZnJvbSBcIm5leHQtcmVkdXgtd3JhcHBlclwiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSZXF1ZXN0Rm9ybSBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIHN0YXRlID0ge1xuICAgIG5hbWU6IFwiXCIsXG4gICAgY3VpdDogXCJcIixcbiAgICBudW1iZXI6IFwiXCJcbiAgfVxuXG4gIGhhbmRsZUNoYW5nZShldmVudCkge1xuICAgIGNvbnN0IGlucHV0ID0gZXZlbnQudGFyZ2V0O1xuXG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBbaW5wdXQubmFtZV06IGlucHV0LnZhbHVlXG4gICAgfSlcbiAgfVxuXG4gIGhhbmRsZVN1Ym1pdChldmVudCkge1xuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICBsZXQgYyA9IHRoaXMuc3RhdGU7XG5cbiAgICBpZiAoYy5uYW1lLmxlbmd0aCA8IDEgfHwgYy5jdWl0Lmxlbmd0aCA8IDEgfHwgYy5udW1iZXIubGVuZ3RoIDwgMSkge1xuICAgICAgcmV0dXJuIGFsZXJ0KCdQb3IgZmF2b3IgbGxlbmEgdG9kb3MgbG9zIGNhbXBvcy4nKVxuICAgIH1cblxuICAgIHRoaXMucHJvcHMub25DcmVhdGVOZXdSZXF1ZXN0KHRoaXMuc3RhdGUpXG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtgZm9ybS1vdmVybGF5ICR7KHRoaXMucHJvcHMuaXNPcGVuKSA/ICdpcy1vcGVuJyA6ICcnfWB9PlxuICAgICAgICA8Zm9ybSBjbGFzc05hbWU9J3JlcXVlc3QtZm9ybScgb25TdWJtaXQ9e3RoaXMuaGFuZGxlU3VibWl0LmJpbmQodGhpcyl9PlxuICAgICAgICAgIDxoMj5DcmVhciBzb2xpY2l0dWQ8L2gyPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybS1maWVsZHNcIj5cblxuICAgICAgICAgICAgPGxhYmVsIGh0bWxGb3I9XCJuYW1lXCI+UmF6b8yBbiBTb2NpYWw8L2xhYmVsPlxuICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgIG5hbWU9XCJuYW1lXCJcbiAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cImVqOiBOZXh1cyBTLkFcIlxuICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5oYW5kbGVDaGFuZ2UuYmluZCh0aGlzKX1cbiAgICAgICAgICAgICAgdmFsdWU9e3RoaXMuc3RhdGUubmFtZX1cbiAgICAgICAgICAgIC8+XG5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmllbGRzLWNvbHNcIj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmaWVsZHMtY29sXCI+XG4gICAgICAgICAgICAgICAgPGxhYmVsIGh0bWxGb3I9XCJjdWl0XCI+TnXMgW1lcm8gZGUgQ1VJVDwvbGFiZWw+XG4gICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICBuYW1lPVwiY3VpdFwiXG4gICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXG4gICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIjAwLTAwMDAwMC0wXCJcbiAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLmhhbmRsZUNoYW5nZS5iaW5kKHRoaXMpfVxuICAgICAgICAgICAgICAgICAgdmFsdWU9e3RoaXMuc3RhdGUuY3VpdH1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZpZWxkcy1jb2xcIj5cbiAgICAgICAgICAgICAgICA8bGFiZWwgaHRtbEZvcj1cIm51bWJlclwiPk51zIFtZXJvIGRlIGVzdGFibGVjaW1pZW50bzwvbGFiZWw+XG4gICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICBuYW1lPVwibnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiMDAtMDAwMDAwLTBcIlxuICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuaGFuZGxlQ2hhbmdlLmJpbmQodGhpcyl9XG4gICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5udW1iZXJ9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZm9ybS1idXR0b25zXCI+XG4gICAgICAgICAgICA8aW5wdXQgdHlwZT1cImJ1dHRvblwiIHZhbHVlPVwiQ2FuY2VsXCIgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICB0aGlzLnByb3BzLm9uQ2xvc2VDcmVhdGVNb2RhbCgpXG4gICAgICAgICAgICB9fSAvPlxuICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJzdWJtaXRcIiB2YWx1ZT1cIkNyZWFyXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICA8L2Zvcm0+XG4gICAgICA8L2Rpdj5cbiAgICApXG4gIH1cbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL2NvbXBvbmVudHMvcmVxdWVzdC1mb3JtLmpzIl0sIm1hcHBpbmdzIjoiO0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7Ozs7Ozs7QUFEQTtBQUNBOztBQUFBO0FBQ0E7QUFEQTtBQUFBOztBQUNBO0FBQ0E7QUFEQTtBQUFBOztBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFHQTtBQUhBO0FBR0E7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFWQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0pBO0FBQ0E7OztBQUFBO0FBQ0E7OztBQUFBO0FBQ0E7OztBQUFBO0FBQ0E7OztBQUFBO0FBQ0E7OztBQUFBO0FBQ0E7Ozs7OztBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUpBO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFLQTs7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUVBO0FBQUE7QUFEQTs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBSEE7Ozs7QUFNQTtBQUFBO0FBR0E7QUFGQTs7OztBQUtBO0FBQUE7QUFHQTtBQUZBOzs7O0FBS0E7QUFBQTs7QUFBQTtBQUVBO0FBRkE7QUFBQTtBQUVBO0FBRUE7QUFGQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUxBO0FBQ0E7QUFLQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUNBO0FBUUE7Ozs7O0FBakRBO0FBQ0E7QUFEQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdkJBO0FBQ0E7Ozs7Ozs7QUFEQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQU5BO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0hBO0FBQ0E7OztBQUFBO0FBQ0E7OztBQUFBO0FBQ0E7Ozs7Ozs7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBSkE7Ozs7O0FBUUE7QUFBQTtBQUdBO0FBRkE7Ozs7QUFLQTtBQUFBO0FBR0E7QUFGQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFGQTtBQUFBO0FBR0E7QUFDQTtBQURBO0FBQUE7O0FBQ0E7QUFBQTtBQUFBO0FBQUE7O0FBQ0E7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUVBO0FBRkE7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUNBOztBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUNBO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOztBQUxBO0FBUUE7QUFSQTtBQUNBO0FBT0E7QUFHQTtBQUhBOzs7Ozs7QUFqRUE7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQXFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUxBO0FBQ0E7Ozs7Ozs7O0FDN0VBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqWkE7QUFDQTs7O0FBQUE7QUFDQTs7Ozs7Ozs7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUZBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBS0E7QUFHQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBREE7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBRkE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTs7QUFMQTtBQVFBO0FBUkE7QUFDQTtBQU9BO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7O0FBTEE7QUFTQTtBQVRBO0FBQ0E7QUFRQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTs7QUFMQTtBQVdBO0FBWEE7QUFDQTtBQVVBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFEQTtBQUFBO0FBR0E7QUFIQTtBQUdBO0FBQUE7QUFNQTtBQU5BOzs7Ozs7QUF4RUE7QUFDQTtBQURBO0FBQ0E7Ozs7O0EiLCJzb3VyY2VSb290IjoiIn0=
            return { page: comp.default }
          })
        