"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _nextReduxWrapper = require("next-redux-wrapper");

var _nextReduxWrapper2 = _interopRequireDefault(_nextReduxWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = "/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/header.js";

exports.default = function (_ref) {
  var onOpenModal = _ref.onOpenModal;
  return _react2.default.createElement("header", { className: "header", __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    }
  }, _react2.default.createElement("div", { className: "header-title", __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    }
  }, _react2.default.createElement("img", { src: "./static/images/visa.png", alt: "", __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    }
  }), _react2.default.createElement("h2", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, "Solicitudes")), _react2.default.createElement("button", { onClick: onOpenModal, __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    }
  }, "Crear Solicitud"));
};