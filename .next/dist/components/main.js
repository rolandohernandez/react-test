'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _header = require('./header');

var _header2 = _interopRequireDefault(_header);

var _request = require('./request');

var _request2 = _interopRequireDefault(_request);

var _requestForm = require('./request-form');

var _requestForm2 = _interopRequireDefault(_requestForm);

var _nextReduxWrapper = require('next-redux-wrapper');

var _nextReduxWrapper2 = _interopRequireDefault(_nextReduxWrapper);

var _reactRedux = require('react-redux');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = '/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/main.js';


var initialRequests = [{
  name: "Nexus tecnologías informáticas S.A.",
  number: 456421654265,
  cuit: "33-70996436-0",
  date: "15/09/2017",
  terminal: ""
}];

// Redux Connect Attempt


var Main = function (_React$Component) {
  (0, _inherits3.default)(Main, _React$Component);

  function Main() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Main);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Main.__proto__ || (0, _getPrototypeOf2.default)(Main)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      requests: initialRequests,
      modalIsOpen: false
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Main, [{
    key: 'onCreateNewRequest',
    value: function onCreateNewRequest(request) {
      var currentRequests = this.state.requests;

      currentRequests.push(request);

      this.setState({
        requests: currentRequests,
        modalIsOpen: false
      });
    }
  }, {
    key: 'onOpenCreateModal',
    value: function onOpenCreateModal() {
      this.setState({
        modalIsOpen: true
      });
    }
  }, {
    key: 'onCloseCreateModal',
    value: function onCloseCreateModal() {
      this.setState({
        modalIsOpen: false
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement('div', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 55
        }
      }, _react2.default.createElement(_header2.default, { onOpenModal: this.onOpenCreateModal.bind(this), __source: {
          fileName: _jsxFileName,
          lineNumber: 57
        }
      }), _react2.default.createElement(_requestForm2.default, {
        isOpen: this.state.modalIsOpen,
        onCloseCreateModal: this.onCloseCreateModal.bind(this),
        onCreateNewRequest: this.onCreateNewRequest.bind(this), __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        }
      }), this.state.requests.map(function (request, index) {
        return _react2.default.createElement(_request2.default, {
          key: index,
          id: index,
          name: request.name,
          number: request.number,
          cuit: request.cuit,
          date: request.date, __source: {
            fileName: _jsxFileName,
            lineNumber: 65
          }
        });
      }));
    }
  }]);

  return Main;
}(_react2.default.Component);

exports.default = Main;