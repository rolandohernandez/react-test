'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _api = require('../static/api.json');

var _api2 = _interopRequireDefault(_api);

var _nextReduxWrapper = require('next-redux-wrapper');

var _nextReduxWrapper2 = _interopRequireDefault(_nextReduxWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = '/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/request.js';


var Request = function (_React$Component) {
  (0, _inherits3.default)(Request, _React$Component);

  function Request() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Request);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Request.__proto__ || (0, _getPrototypeOf2.default)(Request)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      terminal: _this.props.terminal,
      rejecting: false,
      valid: false,
      approved: false,
      rejected: false
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Request, [{
    key: 'approveRequest',
    value: function approveRequest() {
      this.setState({
        approved: true
      });
    }
  }, {
    key: 'rejectRequest',
    value: function rejectRequest() {
      this.setState({
        rejected: true
      });
    }
  }, {
    key: 'handleChange',
    value: function handleChange(event) {
      var input = event.target;

      this.setState((0, _defineProperty3.default)({
        valid: _api2.default.find(function (item) {
          return item.id === event.target.value;
        }.bind(this))
      }, input.name, input.value));
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement('div', { className: 'request ' + (this.state.valid ? 'is-valid' : '') + ' ' + (this.state.rejected ? 'is-rejected' : '') + ' ' + (this.state.approved ? 'is-approved' : ''), __source: {
          fileName: _jsxFileName,
          lineNumber: 37
        }
      }, this.state.rejecting ? _react2.default.createElement('div', { className: 'confirmation-message', __source: {
          fileName: _jsxFileName,
          lineNumber: 40
        }
      }, _react2.default.createElement('p', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 41
        }
      }, '\xBFEsta\u0301s seguro de rechazar la solicitud de este comercio ?'), _react2.default.createElement('div', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 42
        }
      }, _react2.default.createElement('button', { onClick: function onClick() {
          return _this2.setState({ rejecting: false });
        }, __source: {
          fileName: _jsxFileName,
          lineNumber: 43
        }
      }, 'Cancelar'), _react2.default.createElement('button', { onClick: this.rejectRequest.bind(this), __source: {
          fileName: _jsxFileName,
          lineNumber: 44
        }
      }, 'Rechazar'))) : null, _react2.default.createElement('button', { onClick: function onClick() {
          return _this2.setState({ rejecting: !_this2.state.rejecting });
        }, className: 'request-btn request-btn-decline ' + (this.state.rejecting ? 'is-active' : ''), __source: {
          fileName: _jsxFileName,
          lineNumber: 49
        }
      }), _react2.default.createElement('div', { className: 'commerce-details', __source: {
          fileName: _jsxFileName,
          lineNumber: 51
        }
      }, _react2.default.createElement('h3', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 52
        }
      }, this.props.name), _react2.default.createElement('p', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 53
        }
      }, _react2.default.createElement('span', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 53
        }
      }, 'CUIT:'), ' ', this.props.cuit)), _react2.default.createElement('div', { className: 'commerce-details', __source: {
          fileName: _jsxFileName,
          lineNumber: 56
        }
      }, _react2.default.createElement('p', { className: 'commerce-title', __source: {
          fileName: _jsxFileName,
          lineNumber: 57
        }
      }, 'N\xBA de establecimiento'), _react2.default.createElement('p', { className: 'commerce-number', __source: {
          fileName: _jsxFileName,
          lineNumber: 58
        }
      }, this.props.number), _react2.default.createElement('p', { className: 'commerce-date', __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        }
      }, this.props.date)), _react2.default.createElement('input', {
        name: 'terminal',
        type: 'text',
        placeholder: 'N\xBA de Terminal',
        value: this.state.terminal,
        onChange: this.handleChange.bind(this),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 62
        }
      }), _react2.default.createElement('button', { disabled: !this.state.valid, onClick: this.approveRequest.bind(this), className: 'request-btn request-btn-accept', __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        }
      }));
    }
  }]);

  return Request;
}(_react2.default.Component);

exports.default = Request;


Request.defaultProps = {
  id: "",
  name: "",
  number: "",
  date: "11/11/2017",
  cuit: "",
  terminal: ""
};