"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = require("babel-runtime/helpers/defineProperty");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require("babel-runtime/core-js/object/get-prototype-of");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require("babel-runtime/helpers/possibleConstructorReturn");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require("babel-runtime/helpers/inherits");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _nextReduxWrapper = require("next-redux-wrapper");

var _nextReduxWrapper2 = _interopRequireDefault(_nextReduxWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = "/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/request-form.js";


var RequestForm = function (_React$Component) {
  (0, _inherits3.default)(RequestForm, _React$Component);

  function RequestForm() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, RequestForm);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = RequestForm.__proto__ || (0, _getPrototypeOf2.default)(RequestForm)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      name: "",
      cuit: "",
      number: ""
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(RequestForm, [{
    key: "handleChange",
    value: function handleChange(event) {
      var input = event.target;

      this.setState((0, _defineProperty3.default)({}, input.name, input.value));
    }
  }, {
    key: "handleSubmit",
    value: function handleSubmit(event) {
      event.preventDefault();

      var c = this.state;

      if (c.name.length < 1 || c.cuit.length < 1 || c.number.length < 1) {
        return alert('Por favor llena todos los campos.');
      }

      this.props.onCreateNewRequest(this.state);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement("div", { className: "form-overlay " + (this.props.isOpen ? 'is-open' : ''), __source: {
          fileName: _jsxFileName,
          lineNumber: 33
        }
      }, _react2.default.createElement("form", { className: "request-form", onSubmit: this.handleSubmit.bind(this), __source: {
          fileName: _jsxFileName,
          lineNumber: 34
        }
      }, _react2.default.createElement("h2", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 35
        }
      }, "Crear solicitud"), _react2.default.createElement("div", { className: "form-fields", __source: {
          fileName: _jsxFileName,
          lineNumber: 36
        }
      }, _react2.default.createElement("label", { htmlFor: "name", __source: {
          fileName: _jsxFileName,
          lineNumber: 38
        }
      }, "Razo\u0301n Social"), _react2.default.createElement("input", {
        name: "name",
        type: "text",
        placeholder: "ej: Nexus S.A",
        onChange: this.handleChange.bind(this),
        value: this.state.name,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 39
        }
      }), _react2.default.createElement("div", { className: "fields-cols", __source: {
          fileName: _jsxFileName,
          lineNumber: 47
        }
      }, _react2.default.createElement("div", { className: "fields-col", __source: {
          fileName: _jsxFileName,
          lineNumber: 48
        }
      }, _react2.default.createElement("label", { htmlFor: "cuit", __source: {
          fileName: _jsxFileName,
          lineNumber: 49
        }
      }, "Nu\u0301mero de CUIT"), _react2.default.createElement("input", {
        name: "cuit",
        type: "text",
        placeholder: "00-000000-0",
        onChange: this.handleChange.bind(this),
        value: this.state.cuit,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 50
        }
      })), _react2.default.createElement("div", { className: "fields-col", __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        }
      }, _react2.default.createElement("label", { htmlFor: "number", __source: {
          fileName: _jsxFileName,
          lineNumber: 60
        }
      }, "Nu\u0301mero de establecimiento"), _react2.default.createElement("input", {
        name: "number",
        type: "text",
        placeholder: "00-000000-0",
        onChange: this.handleChange.bind(this),
        value: this.state.number,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 61
        }
      })))), _react2.default.createElement("div", { className: "form-buttons", __source: {
          fileName: _jsxFileName,
          lineNumber: 72
        }
      }, _react2.default.createElement("input", { type: "button", value: "Cancel", onClick: function onClick() {
          _this2.props.onCloseCreateModal();
        }, __source: {
          fileName: _jsxFileName,
          lineNumber: 73
        }
      }), _react2.default.createElement("input", { type: "submit", value: "Crear", __source: {
          fileName: _jsxFileName,
          lineNumber: 76
        }
      }))));
    }
  }]);

  return RequestForm;
}(_react2.default.Component);

exports.default = RequestForm;