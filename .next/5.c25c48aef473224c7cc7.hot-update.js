webpackHotUpdate(5,{

/***/ 544:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(46);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(14);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(15);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(47);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(48);

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(11);

var _react2 = _interopRequireDefault(_react);

var _header = __webpack_require__(545);

var _header2 = _interopRequireDefault(_header);

var _request = __webpack_require__(553);

var _request2 = _interopRequireDefault(_request);

var _requestForm = __webpack_require__(555);

var _requestForm2 = _interopRequireDefault(_requestForm);

var _nextReduxWrapper = __webpack_require__(540);

var _nextReduxWrapper2 = _interopRequireDefault(_nextReduxWrapper);

var _reactRedux = __webpack_require__(546);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = '/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/main.js';


var initialRequests = [{
  name: "Nexus tecnologías informáticas S.A.",
  number: 456421654265,
  cuit: "33-70996436-0",
  date: "15/09/2017",
  terminal: ""
}];

// Redux Connect Attempt


var Main = function (_React$Component) {
  (0, _inherits3.default)(Main, _React$Component);

  function Main() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Main);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Main.__proto__ || (0, _getPrototypeOf2.default)(Main)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      requests: initialRequests,
      modalIsOpen: false
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Main, [{
    key: 'onCreateNewRequest',
    value: function onCreateNewRequest(request) {
      var currentRequests = this.state.requests;

      currentRequests.push(request);

      this.setState({
        requests: currentRequests,
        modalIsOpen: false
      });
    }
  }, {
    key: 'onOpenCreateModal',
    value: function onOpenCreateModal() {
      this.setState({
        modalIsOpen: true
      });
    }
  }, {
    key: 'onCloseCreateModal',
    value: function onCloseCreateModal() {
      this.setState({
        modalIsOpen: false
      });
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement('div', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 55
        }
      }, _react2.default.createElement(_header2.default, { onOpenModal: this.onOpenCreateModal.bind(this), __source: {
          fileName: _jsxFileName,
          lineNumber: 57
        }
      }), _react2.default.createElement(_requestForm2.default, {
        isOpen: this.state.modalIsOpen,
        onCloseCreateModal: this.onCloseCreateModal.bind(this),
        onCreateNewRequest: this.onCreateNewRequest.bind(this), __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        }
      }), this.state.requests.map(function (request, index) {
        return _react2.default.createElement(_request2.default, {
          key: index,
          id: index,
          name: request.name,
          number: request.number,
          cuit: request.cuit,
          date: request.date, __source: {
            fileName: _jsxFileName,
            lineNumber: 65
          }
        });
      }));
    }
  }]);

  return Main;
}(_react2.default.Component);

exports.default = Main;

 ;(function register() { /* react-hot-loader/webpack */ if (true) { if (typeof __REACT_HOT_LOADER__ === 'undefined') { return; } if (typeof module.exports === 'function') { __REACT_HOT_LOADER__.register(module.exports, 'module.exports', "/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/main.js"); return; } for (var key in module.exports) { if (!Object.prototype.hasOwnProperty.call(module.exports, key)) { continue; } var namedExport = void 0; try { namedExport = module.exports[key]; } catch (err) { continue; } __REACT_HOT_LOADER__.register(namedExport, key, "/Users/rolandohernandez/Documents/Repositories/Personal/react-test/components/main.js"); } } })();

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiNS5jMjVjNDhhZWY0NzMyMjRjN2NjNy5ob3QtdXBkYXRlLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9tYWluLmpzP2Y3NGMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBIZWFkZXIgZnJvbSAnLi9oZWFkZXInXG5pbXBvcnQgUmVxdWVzdCBmcm9tICcuL3JlcXVlc3QnXG5pbXBvcnQgUmVxdWVzdEZvcm0gZnJvbSAnLi9yZXF1ZXN0LWZvcm0nXG5pbXBvcnQgd2l0aFJlZHV4IGZyb20gJ25leHQtcmVkdXgtd3JhcHBlcidcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCdcblxuY29uc3QgaW5pdGlhbFJlcXVlc3RzID0gW1xuICB7XG4gICAgbmFtZTogXCJOZXh1cyB0ZWNub2xvZ2nMgWFzIGluZm9ybWHMgXRpY2FzIFMuQS5cIixcbiAgICBudW1iZXI6IDQ1NjQyMTY1NDI2NSxcbiAgICBjdWl0OiBcIjMzLTcwOTk2NDM2LTBcIixcbiAgICBkYXRlOiBcIjE1LzA5LzIwMTdcIixcbiAgICB0ZXJtaW5hbDogXCJcIlxuICB9XG5dXG5cbi8vIFJlZHV4IENvbm5lY3QgQXR0ZW1wdFxuXG5cblxuXG5cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTWFpbiBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIHN0YXRlID0ge1xuICAgIHJlcXVlc3RzOiBpbml0aWFsUmVxdWVzdHMsXG4gICAgbW9kYWxJc09wZW46IGZhbHNlXG4gIH1cblxuICBvbkNyZWF0ZU5ld1JlcXVlc3QocmVxdWVzdCkge1xuICAgIGxldCBjdXJyZW50UmVxdWVzdHMgPSB0aGlzLnN0YXRlLnJlcXVlc3RzO1xuXG4gICAgY3VycmVudFJlcXVlc3RzLnB1c2gocmVxdWVzdCk7XG5cbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIHJlcXVlc3RzOiBjdXJyZW50UmVxdWVzdHMsXG4gICAgICBtb2RhbElzT3BlbjogZmFsc2VcbiAgICB9KTtcbiAgfVxuXG4gIG9uT3BlbkNyZWF0ZU1vZGFsKCkge1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgbW9kYWxJc09wZW46IHRydWVcbiAgICB9KTtcbiAgfVxuXG4gIG9uQ2xvc2VDcmVhdGVNb2RhbCgpIHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIG1vZGFsSXNPcGVuOiBmYWxzZVxuICAgIH0pO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiA8ZGl2PlxuXG4gICAgICA8SGVhZGVyIG9uT3Blbk1vZGFsPXt0aGlzLm9uT3BlbkNyZWF0ZU1vZGFsLmJpbmQodGhpcyl9IC8+XG5cbiAgICAgIDxSZXF1ZXN0Rm9ybVxuICAgICAgICBpc09wZW49e3RoaXMuc3RhdGUubW9kYWxJc09wZW59XG4gICAgICAgIG9uQ2xvc2VDcmVhdGVNb2RhbD17dGhpcy5vbkNsb3NlQ3JlYXRlTW9kYWwuYmluZCh0aGlzKX1cbiAgICAgICAgb25DcmVhdGVOZXdSZXF1ZXN0PXt0aGlzLm9uQ3JlYXRlTmV3UmVxdWVzdC5iaW5kKHRoaXMpfSAgLz5cblxuICAgICAge3RoaXMuc3RhdGUucmVxdWVzdHMubWFwKChyZXF1ZXN0LCBpbmRleCkgPT5cbiAgICAgICAgPFJlcXVlc3RcbiAgICAgICAgICBrZXk9e2luZGV4fVxuICAgICAgICAgIGlkPXtpbmRleH1cbiAgICAgICAgICBuYW1lPXtyZXF1ZXN0Lm5hbWV9XG4gICAgICAgICAgbnVtYmVyPXtyZXF1ZXN0Lm51bWJlcn1cbiAgICAgICAgICBjdWl0PXtyZXF1ZXN0LmN1aXR9XG4gICAgICAgICAgZGF0ZT17cmVxdWVzdC5kYXRlfSAvPlxuICAgICAgKX1cbiAgICA8L2Rpdj47XG4gIH1cbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL2NvbXBvbmVudHMvbWFpbi5qcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBOzs7QUFBQTtBQUNBOzs7QUFBQTtBQUNBOzs7QUFBQTtBQUNBOzs7QUFBQTtBQUNBOzs7QUFBQTtBQUNBOzs7Ozs7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFKQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBS0E7Ozs7Ozs7Ozs7Ozs7O0FBQ0E7QUFFQTtBQUFBO0FBREE7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUhBOzs7O0FBTUE7QUFBQTtBQUdBO0FBRkE7Ozs7QUFLQTtBQUFBO0FBR0E7QUFGQTs7OztBQUtBO0FBQUE7O0FBQUE7QUFFQTtBQUZBO0FBQUE7QUFFQTtBQUVBO0FBRkE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFMQTtBQUNBO0FBS0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFDQTtBQVFBOzs7OztBQWpEQTtBQUNBO0FBREE7QUFDQTs7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==