import React from 'react'
import withRedux from "next-redux-wrapper";

export default class RequestForm extends React.Component {
  state = {
    name: "",
    cuit: "",
    number: ""
  }

  handleChange(event) {
    const input = event.target;

    this.setState({
      [input.name]: input.value
    })
  }

  handleSubmit(event) {
    event.preventDefault();

    let c = this.state;

    if (c.name.length < 1 || c.cuit.length < 1 || c.number.length < 1) {
      return alert('Por favor llena todos los campos.')
    }

    this.props.onCreateNewRequest(this.state)
  }

  render() {
    return (
      <div className={`form-overlay ${(this.props.isOpen) ? 'is-open' : ''}`}>
        <form className='request-form' onSubmit={this.handleSubmit.bind(this)}>
          <h2>Crear solicitud</h2>
          <div className="form-fields">

            <label htmlFor="name">Razón Social</label>
            <input
              name="name"
              type="text"
              placeholder="ej: Nexus S.A"
              onChange={this.handleChange.bind(this)}
              value={this.state.name}
            />

            <div className="fields-cols">
              <div className="fields-col">
                <label htmlFor="cuit">Número de CUIT</label>
                <input
                  name="cuit"
                  type="text"
                  placeholder="00-000000-0"
                  onChange={this.handleChange.bind(this)}
                  value={this.state.cuit}
                />
              </div>

              <div className="fields-col">
                <label htmlFor="number">Número de establecimiento</label>
                <input
                  name="number"
                  type="text"
                  placeholder="00-000000-0"
                  onChange={this.handleChange.bind(this)}
                  value={this.state.number}
                />
              </div>
            </div>
          </div>

          <div className="form-buttons">
            <input type="button" value="Cancel" onClick={() => {
              this.props.onCloseCreateModal()
            }} />
            <input type="submit" value="Crear" />
          </div>

        </form>
      </div>
    )
  }
}
