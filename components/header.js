import withRedux from "next-redux-wrapper";

export default ({onOpenModal}) => (
  <header className="header">
    <div className="header-title">
      <img src="./static/images/visa.png" alt=""/>
      <h2>Solicitudes</h2>
    </div>
    <button onClick={onOpenModal}>Crear Solicitud</button>
  </header>
)
