import React from 'react'
import terminalData from '../static/api.json'
import withRedux from "next-redux-wrapper";

export default class Request extends React.Component {
  state = {
    terminal: this.props.terminal,
    rejecting: false,
    valid: false,
    approved: false,
    rejected: false
  };

  approveRequest() {
    this.setState({
      approved: true
    })
  }

  rejectRequest() {
    this.setState({
      rejected: true
    })
  }

  handleChange(event) {
    const input = event.target;

    this.setState({
      valid: terminalData.find(function(item){ return item.id === event.target.value }.bind(this)),
      [input.name]: input.value
    })
  }

  render() {
    return (
      <div className={`request ${(this.state.valid) ? 'is-valid' : ''} ${(this.state.rejected) ? 'is-rejected' : ''} ${(this.state.approved) ? 'is-approved' : ''}`}>

        {(this.state.rejecting) ?
          <div className="confirmation-message">
            <p>¿Estás seguro de rechazar la solicitud de este comercio ?</p>
            <div>
              <button onClick={() => this.setState({ rejecting: false })}>Cancelar</button>
              <button onClick={this.rejectRequest.bind(this)}>Rechazar</button>
            </div>
          </div>
        : null }

        <button onClick={() => this.setState({ rejecting: !this.state.rejecting })} className={`request-btn request-btn-decline ${(this.state.rejecting) ? 'is-active' : ''}`} ></button>

        <div className="commerce-details">
          <h3>{this.props.name}</h3>
          <p><span>CUIT:</span> {this.props.cuit}</p>
        </div>

        <div className="commerce-details">
          <p className="commerce-title">Nº de establecimiento</p>
          <p className="commerce-number">{this.props.number}</p>
          <p className="commerce-date">{this.props.date}</p>
        </div>

        <input
          name="terminal"
          type="text"
          placeholder="Nº de Terminal"
          value={this.state.terminal}
          onChange={this.handleChange.bind(this)}
        />

        <button disabled={!this.state.valid} onClick={this.approveRequest.bind(this)} className="request-btn request-btn-accept"></button>
      </div>
    )
  }
}

Request.defaultProps = {
  id: "",
  name: "",
  number: "",
  date: "11/11/2017",
  cuit: "",
  terminal: ""
};
