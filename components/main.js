import React from 'react';
import Header from './header'
import Request from './request'
import RequestForm from './request-form'
import withRedux from 'next-redux-wrapper'
import { connect } from 'react-redux'

const initialRequests = [
  {
    name: "Nexus tecnologías informáticas S.A.",
    number: 456421654265,
    cuit: "33-70996436-0",
    date: "15/09/2017",
    terminal: ""
  }
]

// Redux Connect Attempt






export default class Main extends React.Component {
  state = {
    requests: initialRequests,
    modalIsOpen: false
  }

  onCreateNewRequest(request) {
    let currentRequests = this.state.requests;

    currentRequests.push(request);

    this.setState({
      requests: currentRequests,
      modalIsOpen: false
    });
  }

  onOpenCreateModal() {
    this.setState({
      modalIsOpen: true
    });
  }

  onCloseCreateModal() {
    this.setState({
      modalIsOpen: false
    });
  }

  render() {
    return <div>

      <Header onOpenModal={this.onOpenCreateModal.bind(this)} />

      <RequestForm
        isOpen={this.state.modalIsOpen}
        onCloseCreateModal={this.onCloseCreateModal.bind(this)}
        onCreateNewRequest={this.onCreateNewRequest.bind(this)}  />

      {this.state.requests.map((request, index) =>
        <Request
          key={index}
          id={index}
          name={request.name}
          number={request.number}
          cuit={request.cuit}
          date={request.date} />
      )}
    </div>;
  }
}
