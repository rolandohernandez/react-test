import Main from '../components/main'
import Head from 'next/head'

export default () => (
  <div>
    <Head>
      <title>React Test</title>
      <meta charSet='utf-8' />
      <link rel="stylesheet" href="/static/main.css" />
      <meta name='viewport' content='initial-scale=1.0, width=device-width' />
      <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet" />
    </Head>

    <Main />
  </div>
)
